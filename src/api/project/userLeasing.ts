import { http } from "@/utils/request";

// 获取所有租赁设备的用户
export function selectNurseUserLeaseList(param: any) {
    return http.get("/nurseUser/selectNurseUserLeaseList", param)
}

// 修改用户租赁信息
export function nurseUserUpdate(param: any) {
    return http.get("/nurseUser/update", param)
}

// 批量删除用户
export function nurseUserDelete(ids: any) {
    return http.post("/nurseUser/delete", undefined, { idList: ids })
}
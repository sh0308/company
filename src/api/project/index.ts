import { http } from "@/utils/request"

export function getDeviceLockInfo(param: any) {
    return http.get("/DeviceLock/getDeviceLockInfo", param)
}
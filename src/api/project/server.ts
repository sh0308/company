import { http } from "@/utils/request";

// 插入一条服务商
export function nurseServerSelectAll(param: any) {
    return http.get("/nurseServer/selectAll", param);
}

// 插入一条服务商
export function nurseServerInsert(param: any) {
    return http.post("/nurseServer/insert", param);
}

// 修改指定服务商
export function nurseServerUpdate(param: any) {
    return http.post("/nurseServer/update", param);
}
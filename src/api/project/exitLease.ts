import { http } from "@/utils/request"

// 查询退租设备
export function getRetreat(param: any) {
    return http.get("/nurseDevice/getRetreat", param)
}

// 审批 退租设备
export function agreeOfCompany(param: any) {
    return http.post("/nurseDevice/agreeOfCompany", param)
}

// 查询所有服务站退租设备
export function getExitApplyList(param: any) {
    return http.get("/exitApply/getExitApplyList", param)
}

// 获取所有可签收货物
export function getSign(param: any) {
    return http.get("/termination/getSign", param)
}

// 审批-回收设备
export function goBack(param: any) {
    return http.post("/termination/goBack", param)
}

// 审批-调拨设备
export function transfer(param: any) {
    return http.post("/termination/transfer", param)
}

// 审批-设备可调度明细列表
export function getTerminationList(param: any) {
    return http.get("/exitApply/getTerminationList", param)
}

// 审批-确认收货
export function signOfCompany(param: any) {
    return http.post("/termination/signOfCompany", param)
}
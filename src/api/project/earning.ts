import { http } from "@/utils/request";

// 查询总公司收益情况
export function selectAllEarnings(param: any) {
    return http.get("/earnings/selectAllEarnings", param)
}

// 运营中心管理查询all收益
export function selectOperatEarnings(param: any) {
    return http.get("/earnings/selectOperatEarnings", param)
}

// 服务站点管理查询all收益
export function selectServerEarnings(param: any) {
    return http.get("/earnings/selectServerEarnings", param)
}

// 获取所有用户收益
export function getUserEarnings(param: any) {
    return http.get("/nurseUser/getUserEarnings", param)
}

// 收益表头汇总数据
export function getSummarizing(param: any) {
    return http.get("/earnings/getSummarizing", param)
}
import { http } from "@/utils/request";


// ============== 商品分类 =================
export function shopCategorySelectAll(param: any) {
    return http.get("/shopCategory/selectAll", param);
}

export function shopCategoryInsert(project: any) {
    return http.post("/shopCategory/insert", project);
}
export function shopCategoryDelete(ids: any) {
    return http.post("/shopCategory/delete", undefined, { idList: ids });
}

export function shopCategoryUpdate(myParam: any) {
    return http.post("/shopCategory/update", myParam);
}

export function shopCategorySelectOne(param: number) {
    return http.get("/shopCategory/" + param);
}

// ============== 商品 =================
export function storeShopSelectAll(param: any) {
    return http.get("/storeShop/selectAll", param);
}

export function storeShopInsert(project: any) {
    return http.post("/storeShop/insert", project);
}
export function storeShopDelete(ids: any) {
    return http.post("/storeShop/delete", undefined, { idList: ids });
}

export function storeShopUpdate(myParam: any) {
    return http.post("/sys/shop//update", myParam);
}

export function storeShopSelectOne(param: number) {
    return http.get("/sys/shop/selectShop?shopId=" + param);
}

// ============== 商品规格分类 =================
export function shopSpecCategorySelectAll(param: any) {
    return http.get("/shopSpecCategory/selectAll", param);
}

export function shopSpecCategoryInsert(project: any) {
    return http.post("/shopSpecCategory/insert", project);
}
export function shopSpecCategoryDelete(ids: any) {
    return http.post("/shopSpecCategory/delete", undefined, { idList: ids });
}

export function shopSpecCategoryUpdate(myParam: any) {
    return http.post("/shopSpecCategory/update", myParam);
}

export function shopSpecCategorySelectOne(param: number) {
    return http.get("/shopSpecCategory/" + param);
}




// ============== 商品规格 =================
export function shopSpecSelectAll(param: any) {
    return http.get("/shopSpec/selectAll", param);
}

export function shopSpecInsert(project: any) {
    return http.post("/shopSpec/insert", project);
}
export function shopSpecDelete(ids: any) {
    return http.post("/shopSpec/delete", undefined, { idList: ids });
}

export function shopSpecUpdate(myParam: any) {
    return http.post("/shopSpec/update", myParam);
}

export function shopSpecSelectOne(param: number) {
    return http.get("/shopSpec/" + param);
}



// ============== 商品规格详细信息 =================
export function shopSpecDescSelectAll(param: any) {
    return http.get("/shopSpecDesc/selectAll", param);
}

export function shopSpecDescInsert(project: any) {
    return http.post("/shopSpecDesc/insert", project);
}
export function shopSpecDescDelete(ids: any) {
    return http.post("/shopSpecDesc/delete", undefined, { idList: ids });
}

export function shopSpecDescUpdate(myParam: any) {
    return http.post("/shopSpecDesc/update", myParam);
}

export function shopSpecDescSelectOne(param: number) {
    return http.get("/shopSpecDesc/" + param);
}




export function sysShopInsert(project: any) {
    return http.post("/sys/shop/insert", project);
}



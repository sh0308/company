import { http } from "@/utils/request";

export function appNoticeSelectAll(param: any) {
    return http.get("/appNotice/selectAll", param);
}

export function appNoticeInsert(project: any) {
    return http.post("/appNotice/insert", project);
}
export function appNoticeDelete(ids: any) {
    return http.post("/appNotice/delete", undefined, { idList: ids });
}

export function appNoticeUpdate(myParam: any) {
    return http.post("/appNotice/update", myParam);
}

export function appNoticeSelectOne(param: number) {
    return http.get("/appNotice/" + param);
}



import { http } from "@/utils/request"

// 获取所有设备操作记录
export function selectAllOperatingRecord(param: any) {
    return http.get("/nurseDevice/selectAllOperatingRecord", param)
}

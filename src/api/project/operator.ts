import { http } from "@/utils/request";

// 运营商查询所有
export function nurseOpertSelectAll(param: any) {
    return http.get("/nurseOpert/selectAll", param);
}

// 添加运营商
export function nurseOpertInsert(param: any) {
    return http.post("/nurseOpert/insert", param);
}

// 修改运营商
export function nurseOpertUpdate(param: any) {
    return http.post("/nurseOpert/update", param);
}
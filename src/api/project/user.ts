import { http } from "@/utils/request"

export function nurseUserSelectAll(param: any) {
    return http.get("/nurseUser/selectAll", param)
}

export function nurseUserInsert(project: any) {
    return http.post("/nurseUser/insert", project)
}
export function nurseUserDelete(ids: any) {
    return http.post("/nurseUser/delete", undefined, { idList: ids })
}

export function nurseUserUpdate(param: any) {
    return http.post("/nurseUser/update", param)
}

// 获取所有护理人数据
export function deviceUserSelectAll(param: any) {
    return http.get("/deviceUser/selectAll", param)
}

// 通过护理人id修改信息
export function deviceUserUpdate(param: any) {
    return http.post("/deviceUser/update", param)
}
import { http } from "@/utils/request";

// 获取所有设备租赁记录
export function nurseDeviceLeaseRecord(param: any) {
    return http.get("/nurseDevice/leaseRecord", param);
}
import { http } from "@/utils/request";

// 设备报表
export function getEquipmentReportInfo(param: any) {
    return http.get("/EquipmentReport/getEquipmentReport", param);
}

// 统计面板
export function getEquipmentReportSum(param: any) {
    return http.get("/EquipmentReport/getEquipmentReportSum", param);
}

// export function appProtocolInsert(project: any) {
//     return http.post("/appProtocol/insert", project);
// }


import { http } from "@/utils/request";

// 根据当前登录用户id获取租户列表
export function nurseDeviceSelectNurseUserList(param: any) {
    return http.get("/nurseUser/selectNurseUserList", param)
}

// 通过租户id修改数据
export function nurseUserUpdate(param: any) {
    return http.post("/nurseUser/update", param)
}

// 通过租户id删除数据
export function nurseUserDelete(param: any) {
    return http.post("/nurseUser/delete", param)
}
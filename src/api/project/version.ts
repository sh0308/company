import { http } from "@/utils/request";

export function appVersionSelectAll(param: any) {
    return http.get("/appVersion/selectAll", param);
}
export function appVersionSelectOne(param: number) {
    return http.get("/appVersion/" + param);
}

export function appVersionInsert(project: any) {
    return http.post("/appVersion/insert", project);
}
export function appVersionDelete(ids: any) {
    return http.post("/appVersion/delete", undefined, { idList: ids });
}

export function appVersionUpdate(myParam: any) {
    return http.post("/appVersion/update", myParam);
}


import { http } from "@/utils/request";

// 获取所有设备
export function nurseDeviceSelectAll(param: any) {
    return http.get("/nurseDevice/queryDeviceList", param);
}

// 查询设备信息
export function getDeviceInfo(param: any) {
    return http.get("/nurseDevice/getDeviceInfo", param);
}

// 添加一台设备
export function nurseDeviceInsert(project: any) {
    return http.post("/nurseDevice/insert", project);
}

// 批量删除设备
export function nurseDeviceDelete(ids: any) {
    return http.post("/nurseDevice/delete", ids);
}

// 修改设备信息
export function nurseDeviceUpdate(myParam: any) {
    return http.post("/nurseDevice/update", myParam);
}

// 将指定设备进行锁机
export function nurseDeviceSendLock(myParam: any) {
    return http.post("/nurseDevice/sendLock", myParam);
}

// 给指定设备进行解锁
export function nurseDeviceSendUnlock(myParam: any) {
    return http.post("/nurseDevice/sendUnlock", myParam);
}

export function deviceTaskSelectAll(param: any) {
    return http.get("/deviceTask/selectAll", param);
}

export function deviceTaskInsert(project: any) {
    return http.post("/deviceTask/insert", project);
}
export function deviceTaskDelete(ids: any) {
    return http.post("/deviceTask/delete", undefined, { idList: ids });
}

export function deviceTaskUpdate(myParam: any) {
    return http.post("/deviceTask/update", myParam);
}

// 设备划拨
export function deviceAllot(myParam: any) {
    return http.post("/nurseDevice/deviceAllot", myParam);
}

// 申请设备记录列表
export function applyDeviceList(param: any) {
    return http.get("/nurseDevice/applyDeviceList", param)
}

// 获取所有运营中心
export function sysUserSelectAllOpentName(param: any) {
    return http.get("/sysUser/selectAllOpentName", param)
}

// 通过运营中心获取下级的所有服务站点
export function sysUserSelectserviceUserById(param: any) {
    return http.get("/sysUser/selectserviceUserById", param)
}

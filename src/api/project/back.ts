import { http } from "@/utils/request";

export function backSelectRent(param: any) {
    return http.get("/back/selectRent", param);
}
export function backSelectOrder(param: any) {
    return http.get("/back/selectOrder", param);
}
export function deviceRentUnbindConfirm(param: any) {
    return http.get("/deviceRent/unbindConfirm", param);
}

export function backSelectIncome(param: any) {
    return http.get("/back/selectIncome", param);
}

export function backSelectRentHeart(param: any) {
    return http.get("/back/selectRentHeart", param);
}
export function backSelectRentHeartStore(param: any) {
    return http.get("/back/selectRentHeartStore", param);
}
export function backSelectRentHeartStoreAll(param: any) {
    return http.get("/back/selectRentHeartStoreAll", param);
}
export function backSelectOrderHeart(param: any) {
    return http.get("/back/selectOrderHeart", param);
}
export function deviceRentUnbindConfirmHeart(param: any) {
    return http.get("/deviceRent/unbindConfirmHeart", param);
}

export function backSelectIncomeHeart(param: any) {
    return http.get("/back/selectIncomeHeart", param);
}
export function backSelectHeart(param: any) {
    return http.get("/back/selectHeart", param);
}
import { http } from "@/utils/request";

// 获取所有推荐关系数据
export function recommendSelectAllRecommend(param: any) {
    return http.get("/recommend/selectAllRecommend", param)
}
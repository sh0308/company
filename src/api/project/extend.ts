import { http } from "@/utils/request";

export function appExtendWelfareSelectAll(param: any) {
    return http.get("/appExtendWelfare/selectAll", param);
}

export function appExtendWelfareInsert(project: any) {
    return http.post("/appExtendWelfare/insert", project);
}
export function appExtendWelfareDelete(ids: any) {
    return http.post("/appExtendWelfare/delete", undefined, { idList: ids });
}

export function appExtendWelfareUpdate(myParam: any) {
    return http.post("/appExtendWelfare/update", myParam);
}

export function appExtendWelfareSelectOne(param: number) {
    return http.get("/appExtendWelfare/" + param);
}



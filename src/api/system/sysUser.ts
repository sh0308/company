import { http } from "@/utils/request";

export function sysUserSelectAll(currentPage: Number, pageSize: Number) {
    return http.get("/sysUser/selectAll", {
        currentPage: currentPage,
        size: pageSize,
    });
}

export function sysUserInsert(project: any) {
    return http.post("/sysUser/insert", project);
}
export function sysUserDelete(ids: any) {
    return http.post("/sysUser/delete", undefined, { idList: ids });
}

export function sysUserUpdate(myParam: any) {
    return http.post("/sysUser/update", myParam);
}

export function sysUserBindByRoleId(userId: any, roleId: any) {
    return http.post("/sysUser/bindByRoleId", undefined, {
        userId: userId,
        roleId: roleId
    });
}

export function Login(userPhone: any, userPassword: any) {
    return http.post("/backstage/login", {
        userPhone: userPhone,
        userPassword: userPassword,
    });
}


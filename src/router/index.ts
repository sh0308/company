import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";

// 1. 新建routers列表
const routerList: RouteRecordRaw[] = [
    {
        path: "/",
        name: "index",
        redirect: "/home",
        component: () => import("@/views/index/index.vue"),
        children: [
            {
                path: "/home",
                name: "Home",
                component: () => import("@/views/index/home/index.vue"),
                meta: {
                    title: "首页"
                }
            },
            {
                path: "/list",
                name: "Device",
                component: () => import("@/views/index/device/list/index.vue"),
                meta: {
                    title: "设备列表"
                }
            },
            {
                path: "/record",
                name: "Record",
                component: () => import("@/views/index/device/record/index.vue"),
                meta: {
                    title: "设备申请记录"
                }
            },
            {
                path: "/transfer",
                name: "Transfer",
                component: () => import("@/views/index/device/transfer/index.vue"),
                meta: {
                    title: "设备调拨记录"
                }
            },
            {
                path: "/operate",
                name: "Operate",
                component: () => import("@/views/index/device/operate/index.vue"),
                meta: {
                    title: "设备操作记录"
                }
            },
            {
                path: "/exception",
                name: "Exception",
                component: () => import("@/views/index/device/exception/index.vue"),
                meta: {
                    title: "异常信息"
                }
            },
            {
                path: "/lock",
                name: "Lock",
                component: () => import("@/views/index/device/lock/index.vue"),
                meta: {
                    title: "锁机信息"
                }
            },
            {
                path: "/lease",
                name: "Lease",
                component: () => import("@/views/index/device/lease/index.vue"),
                meta: {
                    title: "租赁记录"
                }
            },
            {
                path: "/maintain",
                name: "Maintain",
                component: () => import("@/views/index/device/maintain/index.vue"),
                meta: {
                    title: "维修记录"
                }
            },
            {
                path: "/operationCenter",
                name: "OperationCenter",
                component: () => import("@/views/index/center/operationCenter/index.vue"),
                meta: {
                    title: "运营中心管理"
                }
            },
            {
                path: "/serviceCenter",
                name: "ServiceCenter",
                component: () => import("@/views/index/center/serviceCenter/index.vue"),
                meta: {
                    title: "服务中心管理"
                }
            },
            {
                path: "/lesseeList",
                name: "LesseeList",
                component: () => import("@/views/index/users/lesseeList/index.vue"),
                meta: {
                    title: "租户列表"
                }
            },
            {
                path: "/userLeasing",
                name: "UserLeasing",
                component: () => import("@/views/index/users/userLeasing/index.vue"),
                meta: {
                    title: "用户租赁",
                }
            },
            {
                path: "/userExtend",
                name: "UserExtend",
                component: () => import("@/views/index/users/userExtend/index.vue"),
                meta: {
                    title: "推荐列表"
                }
            },
            {
                path: "/exitLease",
                name: "ExitLease",
                component: () => import("@/views/index/users/exitLease/index.vue"),
                meta: {
                    title: "退租管理"
                }
            },
            {
                path: "/income",
                name: "Income",
                component: () => import("@/views/index/earnings/income/index.vue"),
                meta: {
                    title: "收益明细"
                }
            },
            {
                path: "/operationIncome",
                name: "OperationIncome",
                component: () => import("@/views/index/earnings/income/operation.vue"),
                meta: {
                    title: "运营中心收益"
                }
            },
            {
                path: "/serviceIncome",
                name: "ServiceIncome",
                component: () => import("@/views/index/earnings/income/service.vue"),
                meta: {
                    title: "服务中心收益"
                }
            },
            {
                path: "/salesmanIncome",
                name: "SalesmanIncome",
                component: () => import("@/views/index/earnings/income/salesman.vue"),
                meta: {
                    title: "业务员收益"
                }
            },
            {
                path: "/userRevenue",
                name: "UserRevenue",
                component: () => import("@/views/index/earnings/userRevenue/index.vue"),
                meta: {
                    title: "用户收益"
                }
            },
            // {
            //     path: "/user",
            //     name: "User",
            //     component: () => import("@/views/index/user/index.vue"),
            //     meta: {
            //         title: "用户管理",
            //     },
            // },
            // {
            //     path: "/user/desc",
            //     name: "UserDesc",
            //     component: () => import("@/views/index/user/desc.vue"),
            //     meta: {
            //         title: "用户详细信息",
            //         breadcrumbFlag: false,  // 不在面包屑展示
            //     },
            // },
            {
                path: "/protocol",
                name: "Protocol",
                component: () => import("@/views/index/basic/protocol/index.vue"),
                meta: {
                    title: "协议管理",
                },
            },
            {
                path: "/protocol/desc",
                name: "ProtocolDesc",
                component: () => import("@/views/index/basic/protocol/desc.vue"),
                meta: {
                    title: "协议详细信息",
                    breadcrumbFlag: false,  // 不在面包屑展示
                },
            },
            {
                path: "/version",
                name: "Version",
                component: () => import("@/views/index/basic/version/index.vue"),
                meta: {
                    title: "版本管理",
                },
            },
            {
                path: "/version/desc",
                name: "VersionDesc",
                component: () => import("@/views/index/basic/version/desc.vue"),
                meta: {
                    title: "版本详细信息",
                    breadcrumbFlag: false,  // 不在面包屑展示
                },
            },
            {
                path: "/notice",
                name: "Notice",
                component: () => import("@/views/index/basic/notice/index.vue"),
                meta: {
                    title: "通知管理",
                },
            },
            {
                path: "/notice/desc",
                name: "NoticeDesc",
                component: () => import("@/views/index/basic/notice/desc.vue"),
                meta: {
                    title: "通知详细信息",
                    breadcrumbFlag: false,  // 不在面包屑展示
                },
            },
            {
                path: "/news",
                name: "News",
                component: () => import("@/views/index/basic/news/index.vue"),
                meta: {
                    title: "新闻管理",
                },
            },
            {
                path: "/news/desc",
                name: "NewsDesc",
                component: () => import("@/views/index/basic/news/desc.vue"),
                meta: {
                    title: "新闻详细信息",
                    breadcrumbFlag: false,  // 不在面包屑展示
                },
            },
            {
                path: "/extend",
                name: "Extend",
                component: () => import("@/views/index/basic/extend/index.vue"),
                meta: {
                    title: "推广福利",
                }
            },
            {
                path: "/extend/desc",
                name: "ExtendDesc",
                component: () => import("@/views/index/basic/extend/desc.vue"),
                meta: {
                    title: "推广福利详细信息",
                    breadcrumbFlag: false,  // 不在面包屑展示
                }
            },
            {
                path: "/image",
                name: "Image",
                component: () => import("@/views/index/basic/image/index.vue"),
                meta: {
                    title: "图片管理",
                }
            },
            {
                path: "/shop/category",
                name: "ShopCategory",
                component: () => import("@/views/index/shop/category/index.vue"),
                meta: {
                    title: "商品分类",
                }
            },
            {
                path: "/shop/home",
                name: "ShopHome",
                component: () => import("@/views/index/shop/home/index.vue"),
                meta: {
                    title: "商品管理",
                }
            },
            {
                path: "/shop/home/desc",
                name: "ShopHomeDesc",
                component: () => import("@/views/index/shop/home/desc.vue"),
                meta: {
                    title: "商品详细信息",
                    breadcrumbFlag: false,  // 不在面包屑展示
                }
            },
            {
                path: "/shop/search/index",
                name: "SearchIndex",
                component: () => import("@/views/index/shop/search/index.vue"),
                meta: {
                    title: "热门搜索",
                }
            },
            {
                path: "/system/user",
                name: "SystemUser",
                component: () => import("@/views/system/user/index.vue"),
                meta: {
                    title: "系统用户管理",
                }
            },
            {
                path: "/system/role",
                name: "SystemRole",
                component: () => import("@/views/system/role/index.vue"),
                meta: {
                    title: "系统角色管理",
                }
            },
            {
                path: "/system/resource",
                name: "SystemResource",
                component: () => import("@/views/system/resource/index.vue"),
                meta: {
                    title: "系统权限资源管理",
                }
            },
            {
                path: "/logistics",
                name: "Logistics",
                component: () => import("@/views/index/basic/logistics/index.vue"),
                meta: {
                    title: "物流查询",
                }
            },
            {
                path: "/exitLeaseApply",
                name: "ExitLeaseApply",
                component: () => import("@/views/index/device/exitLease/apply.vue"),
                meta: {
                    title: "退租中心调拨申请",
                }
            },
            {
                path: "/signGoods",
                name: "SignGoods",
                component: () => import("@/views/index/device/exitLease/signGoods.vue"),
                meta: {
                    title: "退租申请",
                }
            },
            {
                path: "/report",
                name: "Report",
                component: () => import("@/views/index/device/report/index.vue"),
                meta: {
                    title: "库存管理",
                }
            },
            {
                path: "/repertory",
                name: "Repertory",
                component: () => import("@/views/index/repertory/index.vue"),
                meta: {
                    title: "库存管理",
                }
            }
        ]
    },
    {
        path: "/login",
        name: "Login",
        component: () => import("@/views/login/index.vue")
    },
    {
        path: "/map",
        name: "Map",
        component: () => import("@/views/index/home/map.vue"),
        meta: {
            title: "地图",
        }
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes: routerList,
});

export default router;



import * as echarts from 'echarts'
import { useIndexStore } from "@/store/indexStore"

const chartOneMixin = {
    setup() {
        let myInterval1: any = null;
        let myInterval2: any = null;

        /**
         * 通过视图dom对象节点渲染关于用户在线统计echarts
         * @param {object} ele - dom对象
         */
        function initUserStatusStatistics(ele: any) {
            let option = {
                grid: {
                    left: "0",
                    right: "0",
                    top: "0",
                    containLabel: true
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        lineStyle: {
                            color: '#ddd'
                        }
                    },
                    backgroundColor: 'rgba(255,255,255,1)',
                    padding: [5, 10],
                    textStyle: {
                        color: '#7588E4',
                    },
                    extraCssText: 'box-shadow: 0 0 5px rgba(0,0,0,0.3)'
                },
                xAxis: {
                    type: 'category',
                    data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
                    boundaryGap: false,
                    axisTick: {
                        show: false
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#609ee9'
                        }
                    },
                    axisLabel: {
                        margin: 10,
                        textStyle: {
                            fontSize: 12
                        }
                    }
                },
                yAxis: {
                    type: 'value',
                    splitLine: { show: false },
                    axisTick: {
                        show: false
                    },
                    axisLine: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    }
                },
                series: {
                    type: 'line',
                    smooth: true,
                    showSymbol: false,
                    symbol: 'circle',
                    symbolSize: 6,
                    data: useIndexStore().homeData.left1.monthNewCustomArr,
                    areaStyle: {
                        normal: {
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0,
                                color: 'rgba(137, 189, 27, 0.3)'
                            }, {
                                offset: 0.8,
                                color: 'rgba(137, 189, 27, 0)'
                            }], false),
                            shadowColor: 'rgba(0, 0, 0, 0.1)',
                            shadowBlur: 10
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#f7b851'
                        }
                    },
                    lineStyle: {
                        normal: {
                            width: 3
                        }
                    }
                }
            }

            ele.setOption(option)
        }

        /**
         * 通过视图dom对象节点渲染关于各省份拥有城市运营中心数量统计
         * @param {object} ele - dom对象
         */
        function initOperaStatistics(ele: any) {
            let dataArr = useIndexStore().homeData.left2.yData
            let spaceLength = 5,
                fixedData = [],
                end = 0
            let categoryData = useIndexStore().homeData.left2.xData

            if (categoryData.length < 5) {
                end = categoryData.length - 1
            } else {
                end = 4
            }

            const option = {
                grid: {
                    containLabel: true,
                    top: 20,
                    bottom: -10,
                    left: 0
                },
                xAxis: [
                    {
                        max: 'dataMax',
                        splitLine: { show: false },
                        axisLine: {
                            show: false
                        },
                        axisLabel: {
                            show: false
                        }
                    }
                ],
                yAxis: [
                    {
                        position: 'top',
                        splitLine: { show: false },
                        axisLine: {
                            show: false
                        },
                        axisTick: {
                            show: false
                        },
                        axisLabel: {
                            show: true,
                            textStyle: {
                                color: "#ffff"
                            }
                        },
                        type: 'category',
                        data: categoryData
                    }
                ],
                dataZoom: [
                    {
                        type: 'inside',
                        yAxisIndex: 0,
                        zoomLock: false,
                        width: 8,
                        showDetail: false,
                        startValue: 0, // 从头开始。
                        endValue: 4, // 一次性展示五个。
                        borderWidth: 0,
                        borderColor: 'transparent',
                        backgroundColor: '#343F4B',
                        fillerColor: '#4291CE',
                        showDataShadow: false,
                        brushSelect: false
                    }
                ],
                series: [
                    {
                        name: '',
                        type: 'bar',
                        stack: 'Ad',
                        color: 'transparent',
                        data: fixedData,
                    },
                    {
                        name: '',
                        type: 'bar',
                        stack: 'Ad',
                        barWidth: 20, // 柱子宽度
                        itemStyle: {
                            normal: {
                                color: "#06CAFD"
                            }
                        },
                        label: {
                            show: true,
                            position: 'right',
                            valueAnimation: true,
                            fontWeight: 'bold', // 加粗
                            distance: 10, // 距离
                        },
                        data: dataArr
                    }
                ]
            }

            // 设置定时器, 每隔2s判断是否滚动到末尾, 是则重置为初始状态, 否则向前滚动一位
            if (categoryData.length > 4) {
                if (myInterval1 === null) {
                    myInterval1 = setInterval(() => {
                        if (myInterval1 !== null) {
                            if (option.dataZoom[0].endValue === categoryData.length - 1) {
                                option.dataZoom[0].endValue = end
                                option.dataZoom[0].startValue = 0
                            } else {
                                option.dataZoom[0].endValue = option.dataZoom[0].endValue + 1
                                option.dataZoom[0].startValue = option.dataZoom[0].startValue + 1
                            }
                            ele.setOption(option)
                        }
                    }, 4000)
                }
            }

            ele.setOption(option)
        }

        /**
         * 通过视图dom对象节点渲染关于各城市拥有服务站点数量统计
         * @param {object} ele - dom对象
         */
        function initServerStatistics(ele: any) {
            let dataArr2 = useIndexStore().homeData.left3.yData
            let spaceLength = 5,
                fixedData = [],
                end = 0
            let categoryData2 = useIndexStore().homeData.left3.xData

            if (categoryData2.length < 5) {
                end = categoryData2.length - 1
            } else {
                end = 4
            }

            const option = {
                grid: {
                    containLabel: true,
                    top: 20,
                    bottom: -10,
                    left: 0
                },
                xAxis: [
                    {
                        max: 'dataMax',
                        splitLine: { show: false },
                        axisLine: {
                            show: false
                        },
                        axisLabel: {
                            show: false
                        }
                    }
                ],
                yAxis: [
                    {
                        position: 'top',
                        splitLine: { show: false },
                        axisLine: {
                            show: false
                        },
                        axisTick: {
                            show: false
                        },
                        axisLabel: {
                            show: true,
                            textStyle: {
                                color: "#ffff"
                            }
                        },
                        type: 'category',
                        data: categoryData2
                    }
                ],
                dataZoom: [
                    {
                        type: 'inside',
                        yAxisIndex: 0,
                        zoomLock: false,
                        width: 8,
                        showDetail: false,
                        startValue: 0, // 从头开始。
                        endValue: 4, // 一次性展示五个。
                        borderWidth: 0,
                        borderColor: 'transparent',
                        backgroundColor: '#343F4B',
                        fillerColor: '#4291CE',
                        showDataShadow: false,
                        brushSelect: false
                    }
                ],
                series: [
                    {
                        name: '',
                        type: 'bar',
                        stack: 'Ad',
                        color: 'transparent',
                        data: fixedData,
                    },
                    {
                        name: '',
                        type: 'bar',
                        stack: 'Ad',
                        barWidth: 20, // 柱子宽度
                        itemStyle: {
                            normal: {
                                color: "#06CAFD"
                            }
                        },
                        label: {
                            show: true,
                            position: 'right',
                            valueAnimation: true,
                            fontWeight: 'bold', // 加粗
                            distance: 10, // 距离
                        },
                        data: dataArr2
                    }
                ]
            }

            // 设置定时器, 每隔2s判断是否滚动到末尾, 是则重置为初始状态, 否则向前滚动一位
            if (categoryData2.length > 4) {
                if (myInterval2 == null) {
                    myInterval2 = setInterval(() => {
                        if (myInterval2 !== null) {
                            if (option.dataZoom[0].endValue === categoryData2.length - 1) {
                                option.dataZoom[0].endValue = end
                                option.dataZoom[0].startValue = 0
                            } else {
                                option.dataZoom[0].endValue = option.dataZoom[0].endValue + 1
                                option.dataZoom[0].startValue = option.dataZoom[0].startValue + 1
                            }
                            ele.setOption(option)
                        }
                    }, 5000)
                }
            }

            ele.setOption(option)
        }

        const setMyInterval = () => {
            myInterval1 = null
            myInterval2 = null
        }

        return {
            initUserStatusStatistics,
            initOperaStatistics,
            initServerStatistics,
            setMyInterval,
            myInterval1,
            myInterval2
        }
    }
}




export default chartOneMixin
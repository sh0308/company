import { useIndexStore } from "@/store/indexStore"

const chartTwoMixin = {
    setup() {
        /**
         * 通过视图dom对象节点渲染关于设备年度增长趋势
         * @param {object} ele - dom对象
         */
        function initDeviceStatistics(ele: any) {
            let option = {
                grid: {
                    left: "10px",
                    right: "0",
                    top: "0",
                    containLabel: true
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        lineStyle: {
                            color: '#ddd'
                        }
                    },
                    backgroundColor: 'rgba(255,255,255,1)',
                    padding: [5, 10],
                    textStyle: {
                        color: '#7588E4',
                    },
                    extraCssText: 'box-shadow: 0 0 5px rgba(0,0,0,0.3)'
                },
                xAxis: {
                    type: 'category',
                    data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', "12月"],
                    boundaryGap: false,
                    axisTick: {
                        show: false
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#609ee9'
                        }
                    },
                    axisLabel: {
                        margin: 10,
                        textStyle: {
                            fontSize: 12
                        }
                    }
                },
                yAxis: {
                    type: 'value',
                    splitLine: { show: false },
                    axisTick: {
                        show: false
                    },
                    axisLine: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    }
                },
                series: [{
                    type: 'line',
                    smooth: true,
                    showSymbol: false,
                    symbol: 'circle',
                    symbolSize: 6,
                    data: useIndexStore().homeData.right1.yData,
                    itemStyle: {
                        normal: {
                            color: '#FF6B37'
                        }
                    },
                    lineStyle: {
                        normal: {
                            width: 3
                        }
                    }
                }]
            }

            ele.setOption(option)
        }

        /**
         * 通过视图dom对象节点渲染关于设备状态管理
         * @param {object} ele - dom对象
         */
        function initDeviceStatusStatistics(ele: any) {
            const dataSource = [
                { value: useIndexStore().homeData.right2.onLineSum, name: '在线设备', itemStyle: { color: "#2ECC71" } },
                { value: useIndexStore().homeData.right2.offLineSum, name: '离线设备', itemStyle: { color: "#BAD5FF" } },
                { value: useIndexStore().homeData.right2.lockSum, name: '锁机设备', itemStyle: { color: "#FF5733" } },
                { value: useIndexStore().homeData.right2.unlockSum, name: '未锁设备', itemStyle: { color: "#335BFF" } },
            ]

            let option = {
                tooltip: {
                    trigger: 'item'
                },
                grid: {
                    containLabel: true
                },
                legend: {
                    top: 'center',
                    right: '10%',
                    icon: "circle",
                    orient: 'vertical',
                    itemWidth: 8,
                    textStyle: {
                        color: "#fff"
                    },
                    formatter(name: any) {
                        //找到data中name和文本name值相同的对象
                        const val = dataSource.filter((item: any) => {
                            return item.name === name
                        }) as any
                        return `${name}   ${val[0].value}`
                    }
                },
                series: [
                    {
                        type: 'pie',
                        radius: ['40%', '70%'],
                        center: ['43%', '50%'],
                        avoidLabelOverlap: false,
                        label: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            label: {
                                show: true,
                                fontSize: 40,
                                fontWeight: 'bold'
                            }
                        },
                        labelLine: {
                            show: false
                        },
                        data: dataSource
                    }
                ]
            }

            ele.setOption(option)
        }

        /**
         * 通过视图dom对象节点渲染关于设备状态管理
         * @param {object} ele - dom对象
         */
        function initDeviceRunStatistics(ele: any) {
            const dataSource = [
                { value: useIndexStore().homeData.right3.normalSum, name: '正常数量', itemStyle: { color: "#FFC704" } },
                { value: useIndexStore().homeData.right3.abnormalSum, name: '异常数量', itemStyle: { color: "#FE8945" } },
                { value: useIndexStore().homeData.right3.offLineSum, name: '离线数量', itemStyle: { color: "#06CAFD" } }
            ]

            let option = {
                tooltip: {
                    trigger: 'item'
                },
                grid: {
                    containLabel: true
                },
                legend: {
                    top: 'center',
                    right: '10%',
                    icon: "circle",
                    orient: 'vertical',
                    itemWidth: 8,
                    textStyle: {
                        color: "#fff"
                    },
                    formatter(name: any) {
                        //找到data中name和文本name值相同的对象
                        const val = dataSource.filter((item: any) => {
                            return item.name === name
                        }) as any
                        return `${name}   ${val[0].value}`
                    }
                },
                series: [
                    {
                        type: 'pie',
                        radius: ['40%', '70%'],
                        center: ['43%', '50%'],
                        avoidLabelOverlap: false,
                        label: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            label: {
                                show: true,
                                fontSize: 40,
                                fontWeight: 'bold'
                            }
                        },
                        labelLine: {
                            show: false
                        },
                        data: dataSource
                    }
                ]
            }

            ele.setOption(option)
        }

        return {
            initDeviceStatistics,
            initDeviceStatusStatistics,
            initDeviceRunStatistics
        }
    }
}

export default chartTwoMixin
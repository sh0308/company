export interface propTypes {
    modelValue?: boolean
    title?: string
    active: number | string
    step: Array<any> | any
    inquiry: boolean
}
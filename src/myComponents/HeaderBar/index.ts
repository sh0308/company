import { App } from "vue";
import HeaderBar from "./src/index.vue";
// import MyMenu from "./src/menu"
export default {
  install(app: App) {
    app.component("my-headerBar", HeaderBar);
  },
};

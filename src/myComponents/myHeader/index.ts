import { App } from "vue"
import MyHeader from "./src/index.vue"
export default {
    install(app: App) {
        app.component("my-header", MyHeader)
    }
}
import { App } from "vue"
import MyForm from "./src/index.vue"
export default {
    install(app: App) {
        app.component("my-form", MyForm)
    }
}
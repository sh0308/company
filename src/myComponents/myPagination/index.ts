import { App } from "vue"
import myPagination from "./src/index.vue"
export default {
    install(app: App) {
        app.component("my-Pagination", myPagination)
    }
}
import { isEmpty } from '@/utils';
import { defineStore } from 'pinia';
import { ref } from 'vue';
export const useMessageStore = defineStore('message', () => {

    // 普通个人消息
    let messageInfoList = ref<Array<any>>([]);

    let currentMessageUserId = ref<any>();

    let userOnlineCount = ref(0);


    // 存入消息
    function messageInfoListPush(msg: any) {
        if (currentMessageUserId.value === msg.messageId) {
            messageInfoList.value.push(msg);
        }
        else if (isEmpty(currentMessageUserId.value)) {  // 如果消息id是空也丢掉消息list里面
            messageInfoList.value.push(msg);
        }
    }
    return {
        messageInfoList,
        messageInfoListPush,
        currentMessageUserId,
        userOnlineCount
    }
});
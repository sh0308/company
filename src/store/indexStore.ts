import { defineStore } from "pinia"
import { http } from "@/utils/request"
import { useUserStore } from "@/store/userStore"

export const useIndexStore = defineStore('indexStore', {
    // 为了完整类型推理，推荐使用箭头函数
    state: () => {
        return {
            shrinkFlag: false,
            isHomePage: true,
            homeData: {
                left1: {
                    toMonthAdded: 0, // 当月新增客户数
                    nurseUserSum: 0, // 当前客户总数
                    monthNewCustomArr: []
                },
                left2: {
                    yData: [],
                    xData: []
                },
                left3: {
                    yData: [],
                    xData: []
                },
                centre1: {
                    inuseUserSum: 0, // 在用人数
                    stopUserSum: 0, // 停用人数
                    expireUserSum: 0, // 到期人数
                    onLineSum: 0, // 当前设备在线数量
                    offLineSum: 0, // 当前设备离线数量
                    unRentSum: 0, // 设备待租数量
                    rentSum: 0, // 设备在租数量
                },
                centre2: null,
                right1: {
                    deviceSum: 0, // 设备总数
                    toDayAdded: 0, // 今日新增设备数
                    toMonthAdded: 0, // 当月新增设备数
                    yData: []
                },
                right2: {
                    onLineSum: 0, // 当前设备在线数量
                    offLineSum: 0, // 当前设备离线数量
                    lockSum: 0, // 锁机设备总数
                    unlockSum: 0, // 未锁设备总数
                },
                right3: {
                    offLineSum: 0, // 当前设备离线数量
                    normalSum: 0, // 正常设备数量
                    abnormalSum: 0, // 异常设备数量
                    offRatio: 0, //  未离线占比
                }
            }
        }
    },

    // 计算属性
    getters: {},

    // Action 相当于组件中的 method。它们也是定义业务逻辑的完美选择
    actions: {
        changeShrinkFlagHandle() {
            const dom = document as any
            let element = dom.documentElement as any;
            if (this.shrinkFlag) {
                if (dom.exitFullscreen) {
                    dom.exitFullscreen()
                } else if (dom.webkitCancelFullScreen) {
                    dom.webkitCancelFullScreen()
                } else if (dom.mozCancelFullScreen) {
                    dom.mozCancelFullScreen()
                } else if (dom.msExitFullscreen) {
                    dom.msExitFullscreen()
                }
            } else {
                if (element.requestFullscreen) {
                    element.requestFullscreen()
                } else if (element.webkitRequestFullScreen) {
                    element.webkitRequestFullScreen()
                } else if (element.mozRequestFullScreen) {
                    element.mozRequestFullScreen()
                } else if (element.msRequestFullscreen) {
                    // IE11
                    element.msRequestFullscreen()
                }
            }

            this.shrinkFlag = !this.shrinkFlag
        },
        async initHomeData() {
            const res1: any = await http.get("/back/getNurseSum")
            if (res1.code === 200) {
                this.homeData.centre1.inuseUserSum = res1.data.rows[0].inuseUserSum
                this.homeData.centre1.stopUserSum = res1.data.rows[0].stopUserSum
                this.homeData.centre1.expireUserSum = res1.data.rows[0].expireUserSum
                this.homeData.left1.nurseUserSum = res1.data.rows[0].nurseUserSum
            }

            const res2: any = await http.get("/back/getAllDeviceStatus")
            if (res2.code === 200) {
                this.homeData.centre1.onLineSum = res2.data.rows[0].onLineSum
                this.homeData.centre1.offLineSum = res2.data.rows[0].offLineSum
                this.homeData.centre1.unRentSum = res2.data.rows[0].unRentSum
                this.homeData.centre1.rentSum = res2.data.rows[0].rentSum

                this.homeData.right1.deviceSum = res2.data.rows[0].deviceSum

                this.homeData.right2.onLineSum = res2.data.rows[0].onLineSum
                this.homeData.right2.offLineSum = res2.data.rows[0].offLineSum
                this.homeData.right2.lockSum = res2.data.rows[0].lockSum
                this.homeData.right2.unlockSum = res2.data.rows[0].unlockSum

                this.homeData.right3.offLineSum = res2.data.rows[0].offLineSum
                this.homeData.right3.normalSum = res2.data.rows[0].normalSum
                this.homeData.right3.abnormalSum = res2.data.rows[0].abnormalSum
                let ratio: any = 100 - ((res2.data.rows[0].offLineSum / res2.data.rows[0].deviceSum) * 100)
                if (ratio == 0) {
                    this.homeData.right3.offRatio = 100
                } else {
                    if (/[.]/.test(ratio)) {
                        this.homeData.right3.offRatio = ratio.toFixed(2)
                    } else {
                        this.homeData.right3.offRatio = ratio
                    }
                }
            }

            const res3: any = await http.get("/nurseDevice/newlyIncreasedDevice", { userId: useUserStore().userInfo.userId })
            if (res3.code === 200) {
                this.homeData.right1.toDayAdded = res3.data !== null ? res3.data : 0
            }

            const res4: any = await http.get("/back/getServerDevice")
            if (res4.code === 200) {
                if (res4.data.total > 0) {
                    res4.data.rows.forEach((item: any) => {
                        this.homeData.left2.xData.push(item.serverName)
                        this.homeData.left2.yData.push(item.deviceSum)
                    })
                }
            }

            const res5: any = await http.get("/back/getServerUser")
            if (res5.code === 200) {
                if (res5.data.total > 0) {
                    res5.data.rows.forEach((item2: any) => {
                        this.homeData.left3.xData.push(item2.serverName)
                        this.homeData.left3.yData.push(item2.serverUserSum)
                    })
                }
            }

            const res6: any = await http.get("/nurseDevice/dayList")
            if (res6.code === 200) {
                this.homeData.right1.yData = res6.data
            }

            const res7: any = await http.get("/nurseDevice/newlyIncreasedDeviceByMoth")
            if (res7.code === 200) {
                this.homeData.right1.toMonthAdded = res7.data !== null ? res7.data : 0
            }

            const res8: any = await http.get("/nurseUser/daysByMoth")
            if (res8.code === 200) {
                this.homeData.left1.monthNewCustomArr = res8.data
            }

            const res9: any = await http.get("/nurseUser/newlyIncreasedUserByMoth")
            if (res9.code === 200) {
                this.homeData.left1.toMonthAdded = res9.data !== null ? res9.data : 0
            }
        },
        async intervalViewData() {
            const res1: any = await http.get("/back/getNurseSum")
            if (res1.code === 200) {
                this.homeData.centre1.inuseUserSum = res1.data.rows[0].inuseUserSum
                this.homeData.centre1.stopUserSum = res1.data.rows[0].stopUserSum
                this.homeData.centre1.expireUserSum = res1.data.rows[0].expireUserSum
            }

            const res2: any = await http.get("/back/getAllDeviceStatus")
            if (res2.code === 200) {
                this.homeData.centre1.onLineSum = res2.data.rows[0].onLineSum
                this.homeData.centre1.offLineSum = res2.data.rows[0].offLineSum
                this.homeData.centre1.unRentSum = res2.data.rows[0].unRentSum
                this.homeData.centre1.rentSum = res2.data.rows[0].rentSum

                this.homeData.right2.onLineSum = res2.data.rows[0].onLineSum
                this.homeData.right2.offLineSum = res2.data.rows[0].offLineSum
                this.homeData.right2.lockSum = res2.data.rows[0].lockSum
                this.homeData.right2.unlockSum = res2.data.rows[0].unlockSum

                this.homeData.right3.offLineSum = res2.data.rows[0].offLineSum
                this.homeData.right3.normalSum = res2.data.rows[0].normalSum
                this.homeData.right3.abnormalSum = res2.data.rows[0].abnormalSum
                let ratio: any = 100 - ((res2.data.rows[0].offLineSum / res2.data.rows[0].deviceSum) * 100)
                if (ratio == 0) {
                    this.homeData.right3.offRatio = 100
                } else {
                    if (/[.]/.test(ratio)) {
                        this.homeData.right3.offRatio = ratio.toFixed(2)
                    } else {
                        this.homeData.right3.offRatio = ratio
                    }
                }
            }
        }
    }
})
import { defineStore } from "pinia";
import { ref } from "vue";

// 商品
interface Shop {
    shopId: String;
    shopName: String;
    categoryId: String;
    shopSubName: String;
    shopSort: String;
    shopTag: String;
    shopImg: String;
    shopImgList: String;
    shopInfo: string;
    createdBy: String;
    createdTime: String;
    updatedBy: String;
    updatedTime: String;
    shopDesc: String;
    shopOther: String;
    shopSpecCategoryList: Array<ShopSpecCategory>;
    shopSpecList: Array<ShopSpec>;
}
// 规格的分类
interface ShopSpecCategory {
    specCategoryName: String;
    specSort: String;
    shopSpecDescList: Array<ShopSpecDesc>;
}
// 规格的库存-价格
interface ShopSpec {
    specNum: String;
    specOriginalPrice?: String;
    specPayPrice: String;
    shopSpecDescList: Array<ShopSpecDesc>;
}

// 规格的名称
interface ShopSpecDesc {
    specCategoryId?: String;
    specTitle: String;
}
export const useShopStore = defineStore("useShopStore", () => {
    let currentShop = ref<Shop>({
        shopId: "",
        shopName: "",
        categoryId: "",
        shopSubName: "",
        shopSort: "",
        shopTag: "",
        shopImg: "",
        shopImgList: "",
        shopInfo: "",
        createdBy: "",
        createdTime: "",
        updatedBy: "",
        updatedTime: "",
        shopDesc: "",
        shopOther: "",
        shopSpecCategoryList: [],
        shopSpecList: [],
    });

    const initShop = () => {
        currentShop.value = {
            shopId: "",
            shopName: "",
            categoryId: "",
            shopSubName: "",
            shopSort: "",
            shopTag: "",
            shopImg: "",
            shopImgList: "",
            shopInfo: "",
            createdBy: "",
            createdTime: "",
            updatedBy: "",
            updatedTime: "",
            shopDesc: "",
            shopOther: "",
            shopSpecCategoryList: [
                // {
                //     specCategoryName: "",
                //     specSort: "",
                //     shopSpecDescList: []
                // }
            ],
            shopSpecList: [
                // {
                //     specNum: "",
                //     specOriginalPrice: "",
                //     specPayPrice: "",
                //     shopSpecDescList: []
                // }
            ],
        }
    }
    return { currentShop, initShop }
})
export const arrayGroupBy = (array: Array<any>, f: string) => {
    const groups: any = {}
    array.forEach(function (o) {
        // console.log('f :>> ', f);
        // console.log('o :>> ', o);
        // console.log('o[f] :>> ', o[f]);
        groups[o[f]] = { label: o[f], value: o[f] } || []
    })
    return Object.values(groups);
}